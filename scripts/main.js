function smoothScroll() {
    function a() {
        if (document.body) {
            var a = document.body,
                b = document.documentElement,
                c = window.innerHeight,
                d = a.scrollHeight;
            if (A = document.compatMode.indexOf("CSS") >= 0 ? b : a, l = a, y = !0, top != self) w = !0;
            else if (d > c && (a.offsetHeight <= c || b.offsetHeight <= c)) {
                var e = !1,
                    f = function() {
                        e || b.scrollHeight == document.height || (e = !0, setTimeout(function() {
                            b.style.height = document.height + "px", e = !1
                        }, 500))
                    };
                if (b.style.height = "", setTimeout(f, 10), g("DOMNodeInserted", f), g("DOMNodeRemoved", f), A.offsetHeight <= c) {
                    var h = document.createElement("div");
                    h.style.clear = "both", a.appendChild(h)
                }
            }
            if (document.URL.indexOf("mail.google.com") > -1) {
                var i = document.createElement("style");
                i.innerHTML = ".iu { visibility: hidden }", (document.getElementsByTagName("head")[0] || b).appendChild(i)
            }
            z || v || (a.style.backgroundAttachment = "scroll", b.style.backgroundAttachment = "scroll")
        }
    }

    function b(a, b, c, d) {
        if (d || (d = 1e3), i(b, c), s) {
            var e = +new Date,
                f = e - D;
            if (t > f) {
                var g = (1 + 30 / f) / 2;
                g > 1 && (g = Math.min(g, u), b *= g, c *= g)
            }
            D = +new Date
        }
        if (B.push({
                x: b,
                y: c,
                lastX: 0 > b ? .99 : -.99,
                lastY: 0 > c ? .99 : -.99,
                start: +new Date
            }), !C) {
            var h = a === document.body,
                j = function() {
                    for (var e = +new Date, f = 0, g = 0, i = 0; i < B.length; i++) {
                        var l = B[i],
                            o = e - l.start,
                            q = o >= n,
                            r = q ? 1 : o / n;
                        p && (r = k(r));
                        var s = l.x * r - l.lastX >> 0,
                            t = l.y * r - l.lastY >> 0;
                        f += s, g += t, l.lastX += s, l.lastY += t, q && (B.splice(i, 1), i--)
                    }
                    h ? window.scrollBy(f, g) : (f && (a.scrollLeft += f), g && (a.scrollTop += g)), b || c || (B = []), B.length ? G(j, a, d / m + 1) : C = !1
                };
            G(j, a, 0), C = !0
        }
    }

    function c(c) {
        y || a();
        var d = c.target,
            e = f(d);
        if (!e || c.defaultPrevented || h(l, "embed") || h(d, "embed") && /\.pdf/i.test(d.src)) return !0;
        var g = c.wheelDeltaX || 0,
            i = c.wheelDeltaY || 0;
        g || i || (i = c.wheelDelta || 0), Math.abs(g) > 1.2 && (g *= o / 120), Math.abs(i) > 1.2 && (i *= o / 120), b(e, -g, -i), c.preventDefault()
    }

    function d(a) {
        l = a.target
    }

    function e(a, b) {
        for (var c = a.length; c--;) E[F(a[c])] = b;
        return b
    }

    function f(a) {
        var b = [],
            c = A.scrollHeight;
        do {
            var d = E[F(a)];
            if (d) return e(b, d);
            if (b.push(a), c === a.scrollHeight) {
                if (!w || A.clientHeight + 10 < c) return e(b, document.body)
            } else if (a.clientHeight + 10 < a.scrollHeight && (overflow = getComputedStyle(a, "").getPropertyValue("overflow-y"), "scroll" === overflow || "auto" === overflow)) return e(b, a)
        } while (a = a.parentNode)
    }

    function g(a, b, c) {
        window.addEventListener(a, b, c || !1)
    }

    function h(a, b) {
        return (a.nodeName || "").toLowerCase() === b.toLowerCase()
    }

    function i(a, b) {
        a = a > 0 ? 1 : -1, b = b > 0 ? 1 : -1, (x.x !== a || x.y !== b) && (x.x = a, x.y = b, B = [], D = 0)
    }

    function j(a) {
        var b, c, d;
        return a *= q, 1 > a ? b = a - (1 - Math.exp(-a)) : (c = Math.exp(-1), a -= 1, d = 1 - Math.exp(-a), b = c + d * (1 - c)), b * r
    }

    function k(a) {
        return a >= 1 ? 1 : 0 >= a ? 0 : (1 == r && (r /= j(1)), j(a))
    }
    var l, m = 150,
        n = 800,
        o = 80,
        p = !0,
        q = 8,
        r = 1,
        s = !0,
        t = 10,
        u = 1,
        v = !1,
        w = !1,
        x = {
            x: 0,
            y: 0
        },
        y = !1,
        z = !0,
        A = document.documentElement,
        B = [],
        C = !1,
        D = +new Date,
        E = {};
    setInterval(function() {
        E = {}
    }, 1e4);
    var F = function() {
            var a = 0;
            return function(b) {
                return b.uniqueID || (b.uniqueID = a++)
            }
        }(),
        G = function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || function(a, b, c) {
                window.setTimeout(a, c || 1e3 / 60)
            }
        }();
    g("mousedown", d), g("mousewheel", c), g("load", a)
}


/* Aviv Script */

var favicon;

var owl_loaded = false;

!function($, window) {
    "use strict";
    for (var lastTime = 0, vendors = [ "ms", "moz", "webkit", "o" ], x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"], 
    window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"] || window[vendors[x] + "CancelRequestAnimationFrame"];
    window.requestAnimationFrame || (window.requestAnimationFrame = function(callback) {
        var currTime = new Date().getTime(), timeToCall = Math.max(0, 16 - (currTime - lastTime)), id = window.setTimeout(function() {
            callback(currTime + timeToCall);
        }, timeToCall);
        return lastTime = currTime + timeToCall, id;
    }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    });
    var $doc = $(document), win = $(window), SITE = (window.Modernizr, SITE || {});

    SITE = {
        init: function() {
            var obj, self = this;
            for (obj in self) if (self.hasOwnProperty(obj)) {
                var _method = self[obj];
                void 0 !== _method.selector && void 0 !== _method.init && $(_method.selector).length > 0 && _method.init();
            }
        },
        smoothScroll: {
            selector: ".smooth-scroll",
            init: function() {
                smoothScroll();
            }
        },
        customScroll: {
            selector: ".no-touch .custom-scroll",
            init: function() {
                var base = this, container = $(base.selector);
                container.each(function() {
                    var that = $(this);
                    that.perfectScrollbar({
                        wheelPropagation: !1,
                        suppressScrollX: !0,
                        scrollYMarginOffset: 10,
                        scrollXMarginOffset: 10
                    });
                }), win.resize(function() {
                    base.resize(container);
                });
            },
            resize: function(container) {
                container.perfectScrollbar("update");
            }
        },
        masonry: {
            selector: ".masonry",
            init: function() {
                var base = this, container = $(base.selector);
                container.each(function() {
                    var that = $(this);
                    win.load(function() {
                        that.isotope({
                            itemSelector: ".shop-item",
                            transitionDuration: "0.5s",
                            masonry: {
                                columnWidth: ".shop-item",
                                isFitWidth: false
                            }
                        });
                    });
                });
            }
        },
        mini_cart: {
            selector: ".checkout",
            init: function() {
                var base = this, container = $(base.selector);
                container.find('.checkout__button').on('click', function() {
                    container.addClass('checkout--active');
                    return false;
                });
                container.find('.checkout__cancel').on('click', function() {
                    container.removeClass('checkout--active');
                    return false;
                });
            }
        },
        quick_view: {
            selector: ".quick-view",
            init: function() {
                var base = this, container = $(base.selector);
                var docElem = window.document.documentElement;
                container.each(function() {
                    var that = $(this);
                    that.on('click', function(e) {
                        e.preventDefault();

                        var product = that.parent().parent().parent();

                        var itemsHolder = $('main#content');

                        var itemOffsetTop = parseInt(product.css('top')) + 10;
                        var itemOffsetLeft = parseInt(product.css('left')) + 10;
                        var itemOffsetWidth = parseInt(product.outerWidth()) - 20;
                        var itemOffsetHeight = parseInt(product.outerHeight()) - 20;
                        var holderWidth = parseInt($('main#content').width());

                        var dummy = $('<div></div>');
                        dummy.addClass('placeholder');

                        var transform = 'translate3d(' + itemOffsetLeft + 'px, ' + itemOffsetTop + 'px, 0px) scale3d(' + itemOffsetWidth/holderWidth + ',' + itemOffsetHeight/base.getViewport('y', docElem) + ',1)';

                        // set the width/heigth and position
                        dummy.css('webkitTransform', transform);
                        dummy.css('transform', transform);

                        // add transition class 
                        dummy.addClass('placeholder--trans-in');

                        // insert it after all the grid items
                        itemsHolder.append(dummy);

                        // maincontent overlay
                        itemsHolder.addClass('view-single');

                        setTimeout(function() {
                            // expands the placeholder
                            dummy.css('webkitTransform', 'translate3d(-5px, ' + (base.scrollY(docElem) - 5) + 'px, 0px)');
                            dummy.css('transform', 'translate3d(-5px, ' + (base.scrollY(docElem) - 5) + 'px, 0px)');
                        }, 25);

                        base.onEndTransition(dummy, function() {
                            // add transition class 
                            dummy.removeClass('placeholder--trans-in');
                            dummy.addClass('placeholder--trans-out');

                            // show the main content container
                            $('.product-content').addClass('content--show');
                            $('.product-content .content__item').addClass('content__item--show');

                        }); 

                        return false;
                    });
                });
            },
            /**
             * gets the viewport width and height
             * based on http://responsejs.com/labs/dimensions/
             */
            getViewport: function( axis, docElem ) {
                var client, inner;
                if( axis === 'x' ) {
                    client = docElem['clientWidth'];
                    inner = window['innerWidth'];
                }
                else if( axis === 'y' ) {
                    client = docElem['clientHeight'];
                    inner = window['innerHeight'];
                }

                return client < inner ? inner : client;
            },
            scrollX: function( docElem ) { 
                return window.pageXOffset || docElem.scrollLeft; 
            },
            scrollY: function( docElem ) { 
                return window.pageYOffset || docElem.scrollTop; 
            },
            onEndTransition: function( el, callback ) {
                var support = { transitions: Modernizr.csstransitions },
                    transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
                    transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ];
                var onEndCallbackFn = function( ev ) {
                    if( support.transitions ) {
                        if( ev.target != this ) return;
                        $(this).off( transEndEventName, onEndCallbackFn );
                    }
                    if( callback && typeof callback === 'function' ) { callback.call(this); }
                };
                if( support.transitions ) {
                    $(el).on( transEndEventName, onEndCallbackFn );
                }
                else {
                    onEndCallbackFn();
                }
            }
        },
        animation: {
            selector: ".animation",
            init: function() {
                var base = this, container = $(base.selector);
                base.control(container), win.scroll(function() {
                    base.control(container);
                }); 
            },
            control: function(element) {
                var t = -1;
                element.filter(":in-viewport").each(function() {
                    var that = $(this);
                    t++, setTimeout(function() {
                        that.addClass("animate");
                    }, 200 * t);
                });
            }
        } 
    }, win.resize(function() {
    }), win.scroll(function() {}), $doc.ready(function() {
        SITE.init();
    });
}(jQuery, this);
